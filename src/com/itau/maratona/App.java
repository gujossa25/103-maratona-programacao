package com.itau.maratona;

import java.util.ArrayList;

public class App {

	public static void main(String[] args) {
		Arquivo arquivo = new Arquivo("/home/a2/eclipse-workspace/103-maratona-programacao/src/alunos.csv");
		ArrayList<Aluno> aluno = (ArrayList<Aluno>) arquivo.ler();
		AtribuicaoEquipes atribuicao = new AtribuicaoEquipes();
		ArrayList<Equipe> equipe = atribuicao.sorteador(aluno, 3);
		Impressora.imprimir(equipe);
	}

}
