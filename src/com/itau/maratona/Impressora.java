package com.itau.maratona;

import java.util.ArrayList;

public class Impressora {
	public static void imprimir(int numero) {
		System.out.println(numero);
	}
	public static void imprimir(ArrayList<Equipe> equipe) {
		for (Equipe eq: equipe) {
			System.out.println(eq);
		}
	}
}
