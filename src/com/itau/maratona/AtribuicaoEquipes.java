package com.itau.maratona;

import java.util.ArrayList;

public class AtribuicaoEquipes {
	public static ArrayList<Equipe> sorteador(ArrayList<Aluno> aluno, int qtde) {
		int idEquipe = 1;
		ArrayList<Equipe> equipeList = new ArrayList<>(); 
		Equipe equipe = new Equipe();
		while (aluno.size() > 0){
			int sorteio = sortear(aluno.size());
			equipe.aluno.add(aluno.get(sorteio));
			if (equipe.aluno.size() >= qtde ){
				equipe.id = idEquipe;
				equipeList.add(equipe);
				equipe = new Equipe();
				idEquipe += 1;
			}
			
			aluno.remove(sorteio);
		}
		return equipeList;
	}
	public static int sortear(int tamanho) {
		double resultado = (Math.random() * tamanho);
		return (int) Math.floor(resultado);
	}
	
}
