package com.itau.maratona;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Arquivo {
	private Path caminho;
	
	public Arquivo(String caminho) {
		this.caminho = Paths.get(caminho);
	}
	
	public List<Aluno> ler(){
		List<String> linhas = lerArquivo();
		List<Aluno> alunos = new ArrayList<>();
		
		for(String linha : linhas) {
			String[] partes = linha.split(";");
			
			Aluno aluno = new Aluno();
			aluno.nome = partes[0];
			aluno.cpf = partes[1];
			aluno.email = partes[2];
			
			alunos.add(aluno);
		}
		
		return alunos;
	}
	
	private List<String> lerArquivo(){
		List<String> lista;
		
		try {
			lista = Files.readAllLines(caminho);
		} catch (IOException e) {
			return null;
		}
		
		return lista;
	}

}
