package com.itau.maratona;

public class Aluno {
	public String nome;
	public String cpf;
	public String email;
	public String toString() {
		String resultado = "aluno ";
		resultado += "cpf: " + cpf + " ";
		resultado += "email: "+ email + " ";
		resultado += "nome: "+ nome +"\n";
		
		return resultado;
	}
}
